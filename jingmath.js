jing.math = function () {
        
	var addab = function(a,b){
		return a + b;
	}

	var subtractab = function(a,b){
		return a - b;
	}

	var multiplyab = function(a,b){
		return a * b;
	}

	var divideab = function(a,b){
		return a / b;
	}

	var modulusab = function(a,b){
		return a % b;
	}
};